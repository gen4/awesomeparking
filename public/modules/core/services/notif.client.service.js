'use strict';

angular.module('core').service('Notif', ['$interval',
	function($interval) {


		this.messages = [];
		this.lastMessage= [];

		this.interval;

		this.log = function(msg){
			this.post('log',msg);
		};
		this.error = function(msg){
			this.post('error',msg);
		};
		this.success = function(msg){
			this.post('success',msg);
		};

		this.post = function(type,msg){
			console.log('Notification:',msg)
			this.messages.push({type:type,msg:msg,id:this.getId()});
			this.setLastMessage();
			this.resetInterval();
		};

		this.getId= function(){
			var a = new Date().getTime();
			var b = Math.round(Math.random()*999999)
			return a+'_'+b;
		};

		this.resetInterval = function(){
			var that = this;
			$interval.cancel(this.interval);
			this.interval = $interval(function(){that.killLastMessage()}, 3000,1);
		};


		this.killLastMessage = function(){
			if(this.messages && this.messages.length>0){
				this.messages.shift();
				this.setLastMessage();
				this.resetInterval();
			}
		};

		this.closeMessage = function(message){
			_(this.messages).remove(function(item){
				return message.id == item.id;
			})
			this.setLastMessage();
		}

		this.setLastMessage = function(){
			//this.lastMessage = this.messages.length>0?[this.messages[this.messages.length-1]]:[];
			this.lastMessage = [this.messages[this.messages.length-1]];
		}
	}
]);
