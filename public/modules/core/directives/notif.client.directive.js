'use strict';

angular.module('core').directive('notif', ['Notif','$animate',
	function(Notif,$animate) {
		return {
			templateUrl: '/modules/core/views/templates/notif.tpl.html',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Notif directive logic
				// ...

				element.css({
					'z-index': 99960,
					position: 'fixed',
					top:0,
					width:'100%',
					right:0
				});
				scope.notif = Notif;
			}
		};
	}
]);
