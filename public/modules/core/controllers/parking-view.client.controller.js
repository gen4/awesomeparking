'use strict';

angular.module('core').controller('ParkingViewController', ['$scope','$modal','$log','$http','$q','Notif','ngTableParams','$filter',
	function($scope,$modal,$log,$http,$q,Notif,ngTableParams,$filter) {



		$scope.tableParams = new ngTableParams({
			page: 0,            // show first page
			count: 10,          // count per page
			filter: {
			}

		}, {
			total: 0, // length of data
			counts: [],
			getData: function($defer, params) {
				console.log('get data',params)
				$http({
					url: '/parkings',
					method: "GET",
					params:{
						skip:params.page()*params.count(),
						limit:params.count()
					}
				})
					.success(function(data){
						console.log(data);
						params.total(data.results.length);
						$defer.resolve(data.results);
					})
					.error(function(data){
						console.error(data);
					})
			}
		});




		$scope.refreshParkings = function () {
			$scope.refreshInProgress=true;
				$http.post('/parkings/refresh')
					.success(function(data){
						console.log(data);
						Notif.success('Парковки обновлены');
						$scope.tableParams.reload();
						$scope.refreshInProgress=false;
					})
					.error(function(data){
						console.error(data);
						Notif.error(data.text);
						$scope.refreshInProgress=false;
					})

		};

	}
]).controller('ModalAddParkingCtrl', function ($scope, $modalInstance, items) {


	$scope.ok = function () {
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});
