'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication','$stateParams','$http','$location',
	function($scope, Authentication,$stateParams,$http,$location) {
		// This provides Authentication context.
		$scope.authentication = Authentication;

		$scope.view={mode:'list'};

		$scope.menu = [
			{id:'sensors', label:"Датчики", badge:'!'},
			{id:'parkings', label:"Паркинги" },
			{id:'boards', label:"Доски"},
		];

		$scope.gotoState = function(item){
			$location.path('/'+item.id+'/'+$scope.view.mode)
		};

		$scope.state = $stateParams;

	}
]);
