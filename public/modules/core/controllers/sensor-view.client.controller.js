'use strict';

angular.module('core').controller('SensorViewController', ['$scope','$modal','$log','$http','$q','Notif','ngTableParams','$filter',
	function($scope,$modal,$log,$http,$q,Notif,ngTableParams,$filter) {
		$scope.person = {};

		$scope.tableParams = new ngTableParams({
			page: 0,            // show first page
			count: 10	        // count per page
		}, {
			total: 0, // length of data
			counts: [],
			groupBy: 'parking_id',
			getData: function($defer, params) {
				console.log('get data',params)
				$http({
						url: '/sensors',
						method: "GET",
						params:{
							skip:params.page()*params.count(),
							limit:params.count()
						}
					})
					.success(function(data){
						console.log(data);
						params.total(data.total);
						$defer.resolve(data.results);
					})
					.error(function(data){
						console.error(data);
					})
			}
		});



		$scope.openAddSensor = function () {

			var modalInstance = $modal.open({
				templateUrl: '/modules/core/views/modals/addSensor.tpl.html',
				controller: 'ModalAddSensorCtrl'
				//resolve: {
				//	parkings: function () {
				//		var deferred = $q.defer();
				//
				//
				//	}
				//}
			});

			modalInstance.result.then(function (data) {
				$http.post('/sensors/create',data)
					.success(function(data){
						console.log(data);
						Notif.success('Датчик добавлен');
						$scope.tableParams.reload();
					})
					.error(function(data){
						console.error(data);
						Notif.error(data.text);
					})
			}, function () {
				$log.info('Modal dismissed at: ' + new Date());
			});
		};



	}
]).controller('ModalAddSensorCtrl', function ($scope, $modalInstance,$http) {

	$scope.parkings = [];
	$scope.data = {};

	$scope.onSelect = function(a,b){
		$scope.data.parking_id=b;
	}

	$http.get('/parkings')
		.success(function(data){
			console.log(data)
			_(data.objects).each(function(item){
				console.log(item.address);
				if(!item.address){item.address = ' - ';console.log('\tok..')}
			});
			$scope.parkings = data.results;
		})
		.error(function(data){
			console.log(data)
		});


	$scope.ok = function () {

		console.log($scope.data)
		$modalInstance.close($scope.data);
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
});
