'use strict';

module.exports = function(app) {
	// Routing logic   
	var parking = require('../../app/controllers/parking');
	app.route('/parking/create').post(parking.create);
	app.route('/parkings/refresh').post(parking.refresh);
	app.route('/parkings').get(parking.read);

};
