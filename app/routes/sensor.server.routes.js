'use strict';

module.exports = function(app) {
	var sensor = require('../../app/controllers/sensor');
	app.route('/sensors/create').post(sensor.create);
	app.route('/sensors').get(sensor.read);
};
