'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Parking Schema
 */
var ParkingSchema = new Schema({
	_id: {
		type: String,
		required: true,
		unique: true,
		index: true
	},
	spaces:{
		total: {
			type: Number,
			default: 0
		}
	},
	category:{
		type:Number
	},
	description:{
		type: String
	},
	address:{
		type:String
	},
	fetchedAt:{
		type:Date,
		default:Date.now
	}

},{_id:false});

mongoose.model('Parking', ParkingSchema);
