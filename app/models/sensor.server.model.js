'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Sensor Schema
 */
var SensorSchema = new Schema({
	_id:{
		type: String,
		required:true,
		unique:true,
		index:true
	},
	base_id: {
		type: String,
		required:true
	},
	parking_id: {
		type: String,
		ref: 'Parking',
		index: true
	},
	status:{
		type:Number
	},
	battery:{
		type:Number
	},
	last_sync:{
		type:Date,
		default:Date.now
	}

},{_id:false});

mongoose.model('Sensor', SensorSchema);
