'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Sensor         = mongoose.model('Sensor'),
    async          = require('async'),
    _ = require('lodash');

/**
 * Create a Sensor
 */
exports.create = function(req, res) {
    //req.body._id = new ObjectId(req.body._id);
    var sensor = new Sensor(req.body);
    sensor.save(function(err,s){
        if(err){
            console.error(err);
            res.status(500).json({error:{text:'ошибка сервера'}});
        }else{
            res.status(200).json(s);
        }
    })
};

/**
 * Show the current Sensor
 */
exports.read = function(req, res) {

    async.parallel({
        count:function(_cb){
            Sensor.count({}).exec(_cb)
        },
        results:function(_cb){
            console.log('LS ',req.query)
            Sensor.find({}).limit(req.query.limit || 30).skip(req.query.skip || 0).lean().exec(_cb);
        }
    },function(err,data){

        if(err){
            res.status(500).json({error:{text:'ошибка на сервер'}});
        } else{
            res.status(200).json(data);
        }
    })

};

/**
 * Update a Sensor
 */
exports.update = function(req, res) {

};

/**
 * Delete an Sensor
 */
exports.delete = function(req, res) {

};

/**
 * List of Sensors
 */
exports.list = function(req, res) {

};
