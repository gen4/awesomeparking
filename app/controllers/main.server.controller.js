'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    _ = require('lodash');

/**
 * Create a Main
 */
exports.create = function(req, res) {

};

/**
 * Show the current Main
 */
exports.read = function(req, res) {

};

/**
 * Update a Main
 */
exports.update = function(req, res) {

};

/**
 * Delete an Main
 */
exports.delete = function(req, res) {

};

/**
 * List of Mains
 */
exports.list = function(req, res) {

};