'use strict';

/**
 * Module dependencies.
 */
var mongoose        = require('mongoose'),
    Parking         = mongoose.model('Parking'),
    needle         = require('needle'),
    async          = require('async'),
    _ = require('lodash');




function saveParking(item,callback){
    console.log('.r \tsave p.');
    async.waterfall(
        [
            function(_cb){
                Parking.findOne({_id:item._id},_cb)
            },
            function(localParking,_cb){
                if(localParking){
                    //update
                    console.log('.r \tsave p.\t update',item._id);
                    var id = item._id;
                    delete item._id;
                    Parking.findByIdAndUpdate(id,item,_cb);
                }else{
                    //create new
                    console.log('.r \tsave p.\t create new: ',item._id);
                    var p = new Parking(item);
                    p.save(_cb)
                }
            }
        ],callback);
}






/**
 * Create a Parking
 */
exports.create = function(req, res) {
    //req.body._id = new ObjectId(req.body._id);
    var parking = new Parking(req.body);
    parking.save(function(err,parking){
        if(err){
            console.error(err);
            res.status(500).json({error:{text:'ошибка сервера'}});
        }else{
            res.status(200).json(parking);
        }
    })
};




/**
 * fetch Parkings list
 */
exports.refresh = function(req, res) {
    var data = {
        types      : 'parking'
    }
    async.waterfall([
        function(_cb){
            //fetch parkings from api
            console.log('.r fetching');
            needle.request('get','https://belparking.fitdev.ru/api/objects',data,function(err,response){
                if(err) {
                    _cb(err)
                }else if(!response || response.statusCode!=200 || !response.body){
                    _cb('wrong response')
                }else {
                    _cb(null,response.body)
                }
            });
        },
        function(fetched,_cb){
            //save fetched
            console.log('.r fetch done');
            if(fetched  && fetched.objects && fetched.objects.length>0){
                async.eachLimit(fetched.objects,10,saveParking,_cb)
            }else{
                console.warn('Parking refresh(). no objects recieved')
                _cb(null);
            }

        }
    ],function(err){
        console.log('.r all done');
        if (err) {
            console.error('parking.server download()', err);
            res.status(500).json({error: {text: 'ошибка на сервере'}});
        }else{
            res.status(200).json({status:'success'});
        }
    })
};







/**
 * Show the current Parking
 */
exports.read = function(req, res) {
    async.parallel({
        count:function(_cb){
            Parking.count({}).exec(_cb)
        },
        results:function(_cb){
            Parking.find({}).limit(req.query.limit || 200).skip(req.query.skip || 0).lean().exec(_cb);
        }
    },function(err,data){

        if(err){
            res.status(500).json({error:{text:'ошибка на сервер'}});
        } else{
            res.status(200).json(data);
        }
    })
};

/**
 * Update a Parking
 */
exports.update = function(req, res) {

};

/**
 * Delete an Parking
 */
exports.delete = function(req, res) {

};

/**
 * List of Parkings
 */
exports.list = function(req, res) {

};
