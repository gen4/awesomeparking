

# Parking
Основой формат: **JSON**
Успешный ответ: **200**
Ошибка: **300, 400 или 500**


## Датчики
#### [POST] **/:operatorID/sensors/**
меняет данные о существующих датчиках и добавляет новые, если таких нет.
Ждет список датчиков в формате




```javascript
{
	id:"ASD",
	parking_id:"XXX",
	base_id:"XXX",
	status:0,
	battery:2,
	last_sync:19232334434
}

```

#### [POST] **/sensors**
возвращает все сенсоры мира
на выходе:

```javascript
{
	total:2800
	results:[
	{...},
	{...},
	...å
	]
}
```




## Паркинги
#### [POST] **/parking/create**
создание нового паркинга
ждет объект

```javascript
{
	id:"XXX",
	num:22,
	address:"XXX",
	count:24,
	description:"XXX",
	last_sync:19232334434
}
```
#### [POST] **/parkings**
возвращает все парковки мира
лимит 200




