'use strict';

module.exports = {
	app: {
		title: 'B - Parking',
		description: 'BP',
		keywords: 'Parking'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'MEAN',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				//'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/custom_libs/bootstrap_theme/css/bootstrap.min.css',
				'public/lib/angular-ui-select/dist/select.min.css',
				'public/lib/ng-table/ng-table.min.css'
			],
			js: [
				'public/lib/angular/angular.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-cookies/angular-cookies.js', 
				'public/lib/angular-animate/angular-animate.js', 
				'public/lib/angular-touch/angular-touch.js', 
				'public/lib/angular-sanitize/angular-sanitize.js', 
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-ui-select/dist/select.min.js',
				'public/lib/lodash/dist/lodash.min.js',
				'public/lib/ng-table/ng-table.min.js',


				'public/lib/moment/min/moment.min.js',
				'public/lib/moment/locale/ru.js',
				'public/lib/angular-moment/angular-moment.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js'

			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		fonts: [
			'public/modules/*/fonts/*.*',
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
